FROM node:20 as build
WORKDIR /app
COPY package.json ./
RUN npm install -g @angular/cli
RUN npm install
COPY . .
RUN ng build 

FROM nginx:alpine
COPY --from=build /app/dist/frontend-demo/browser/ /usr/share/nginx/html